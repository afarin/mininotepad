
import java.awt.Color;
import java.awt.Font;
import java.awt.GraphicsEnvironment;
import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.StringSelection;
import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Scanner;
import javax.swing.DefaultListModel;
import javax.swing.JColorChooser;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.KeyStroke;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.text.BadLocationException;
import javax.swing.text.DefaultHighlighter;
import javax.swing.text.Document;
import javax.swing.text.Highlighter;
import javax.swing.text.JTextComponent;

/**
 *
 * @author Afarin
 */
public class TextEditor extends javax.swing.JFrame {
    
    private String defaultTitle = "Untitled-Notepad";
    private String title = defaultTitle;
    private boolean isModified = false; // textAtrea content modified or not
    File currentFile;
    String file;
    final static String FILE_NAME = "notepad.txt";
    Clipboard clipboard = getToolkit().getSystemClipboard();
    DefaultListModel<String> fontListModel = new DefaultListModel<>();
    DefaultListModel<String> sizeListModel = new DefaultListModel<>();
    DefaultListModel<String> styleListModel = new DefaultListModel<>();
    String[] fonts = GraphicsEnvironment.getLocalGraphicsEnvironment().getAvailableFontFamilyNames();
    String[] sizes = {"8", "10", "12", "16", "18", "20", "24", "28", "32", "48", "72"};
    String[] styles = {"Regular", "Bold", "Italic", "Bold Italic"};
    
    public TextEditor() {
        initIcon(); //set icon for JFrame
        initComponents();
        this.setTitle(defaultTitle);
        FileNameExtensionFilter filter = new FileNameExtensionFilter("Text files (*.txt,*.text)",
                "txt", "text");
        fileChooser.setFileFilter(filter);
        
    }
    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        fileChooser = new javax.swing.JFileChooser();
        dlgFont = new javax.swing.JDialog();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jScrollPane2 = new javax.swing.JScrollPane();
        dlgFont_lstFont = new javax.swing.JList<>();
        jScrollPane3 = new javax.swing.JScrollPane();
        dlgFont_lstFontStyle = new javax.swing.JList<>();
        jScrollPane4 = new javax.swing.JScrollPane();
        dlgFont_lstFontSize = new javax.swing.JList<>();
        dlgFont_txtPreview = new javax.swing.JTextField();
        dlgFont_btnOk = new javax.swing.JButton();
        dlgFont_btnCancel = new javax.swing.JButton();
        colorChooser = new javax.swing.JColorChooser();
        dlgFind = new javax.swing.JDialog();
        jLabel4 = new javax.swing.JLabel();
        dlgFind_tfFindWhat = new javax.swing.JTextField();
        dlgFind_btnFind = new javax.swing.JButton();
        lblShowPath = new javax.swing.JLabel();
        btnSearch = new javax.swing.JButton();
        tfSearch = new javax.swing.JTextField();
        jScrollPane1 = new javax.swing.JScrollPane();
        taTextArea = new javax.swing.JTextArea();
        jmbMenu = new javax.swing.JMenuBar();
        mFile = new javax.swing.JMenu();
        miNew = new javax.swing.JMenuItem();
        miOpen = new javax.swing.JMenuItem();
        miSave = new javax.swing.JMenuItem();
        miSaveAs = new javax.swing.JMenuItem();
        miExit = new javax.swing.JMenuItem();
        mEdit = new javax.swing.JMenu();
        miCut = new javax.swing.JMenuItem();
        miCopy = new javax.swing.JMenuItem();
        miPaste = new javax.swing.JMenuItem();
        miFind = new javax.swing.JMenuItem();
        mFormat = new javax.swing.JMenu();
        miColor = new javax.swing.JMenuItem();
        miFont = new javax.swing.JMenuItem();

        dlgFont.setTitle("Choose Font");
        dlgFont.addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowActivated(java.awt.event.WindowEvent evt) {
                dlgFontWindowActivated(evt);
            }
        });

        jLabel1.setText("Font:");

        jLabel2.setText("Font Style:");

        jLabel3.setText("Font Size");

        dlgFont_lstFont.setModel(fontListModel);
        dlgFont_lstFont.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        dlgFont_lstFont.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                dlgFont_lstFontMouseClicked(evt);
            }
        });
        dlgFont_lstFont.addListSelectionListener(new javax.swing.event.ListSelectionListener() {
            public void valueChanged(javax.swing.event.ListSelectionEvent evt) {
                dlgFont_lstFontValueChanged(evt);
            }
        });
        jScrollPane2.setViewportView(dlgFont_lstFont);

        dlgFont_lstFontStyle.setModel(styleListModel);
        dlgFont_lstFontStyle.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        dlgFont_lstFontStyle.addListSelectionListener(new javax.swing.event.ListSelectionListener() {
            public void valueChanged(javax.swing.event.ListSelectionEvent evt) {
                dlgFont_lstFontStyleValueChanged(evt);
            }
        });
        jScrollPane3.setViewportView(dlgFont_lstFontStyle);

        dlgFont_lstFontSize.setModel(sizeListModel);
        dlgFont_lstFontSize.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        dlgFont_lstFontSize.addListSelectionListener(new javax.swing.event.ListSelectionListener() {
            public void valueChanged(javax.swing.event.ListSelectionEvent evt) {
                dlgFont_lstFontSizeValueChanged(evt);
            }
        });
        jScrollPane4.setViewportView(dlgFont_lstFontSize);

        dlgFont_txtPreview.setEditable(false);
        dlgFont_txtPreview.setText("AaBbCcDdEeFfGgHh");
        dlgFont_txtPreview.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                dlgFont_txtPreviewActionPerformed(evt);
            }
        });

        dlgFont_btnOk.setText("Ok");
        dlgFont_btnOk.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                dlgFont_btnOkActionPerformed(evt);
            }
        });

        dlgFont_btnCancel.setText("Cancel");
        dlgFont_btnCancel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                dlgFont_btnCancelActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout dlgFontLayout = new javax.swing.GroupLayout(dlgFont.getContentPane());
        dlgFont.getContentPane().setLayout(dlgFontLayout);
        dlgFontLayout.setHorizontalGroup(
            dlgFontLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(dlgFontLayout.createSequentialGroup()
                .addGroup(dlgFontLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(dlgFontLayout.createSequentialGroup()
                        .addGap(24, 24, 24)
                        .addGroup(dlgFontLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 183, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(dlgFontLayout.createSequentialGroup()
                                .addGap(11, 11, 11)
                                .addComponent(jLabel1)))
                        .addGap(34, 34, 34)
                        .addGroup(dlgFontLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel2)
                            .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 168, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(48, 48, 48)
                        .addGroup(dlgFontLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jScrollPane4, javax.swing.GroupLayout.PREFERRED_SIZE, 168, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel3)))
                    .addGroup(dlgFontLayout.createSequentialGroup()
                        .addGap(149, 149, 149)
                        .addComponent(dlgFont_txtPreview, javax.swing.GroupLayout.PREFERRED_SIZE, 404, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(dlgFontLayout.createSequentialGroup()
                        .addGap(223, 223, 223)
                        .addComponent(dlgFont_btnOk)
                        .addGap(44, 44, 44)
                        .addComponent(dlgFont_btnCancel)))
                .addContainerGap(40, Short.MAX_VALUE))
        );

        dlgFontLayout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {jLabel1, jLabel2, jLabel3});

        dlgFontLayout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {dlgFont_btnCancel, dlgFont_btnOk});

        dlgFontLayout.setVerticalGroup(
            dlgFontLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(dlgFontLayout.createSequentialGroup()
                .addGap(40, 40, 40)
                .addGroup(dlgFontLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(jLabel2)
                    .addComponent(jLabel3))
                .addGap(18, 18, 18)
                .addGroup(dlgFontLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jScrollPane2)
                    .addComponent(jScrollPane3)
                    .addComponent(jScrollPane4, javax.swing.GroupLayout.DEFAULT_SIZE, 251, Short.MAX_VALUE))
                .addGap(54, 54, 54)
                .addComponent(dlgFont_txtPreview, javax.swing.GroupLayout.PREFERRED_SIZE, 78, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(41, 41, 41)
                .addGroup(dlgFontLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(dlgFont_btnOk)
                    .addComponent(dlgFont_btnCancel))
                .addContainerGap(35, Short.MAX_VALUE))
        );

        dlgFind.setTitle("Find");

        jLabel4.setText("Find What:");

        dlgFind_btnFind.setText("Find");
        dlgFind_btnFind.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                dlgFind_btnFindActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout dlgFindLayout = new javax.swing.GroupLayout(dlgFind.getContentPane());
        dlgFind.getContentPane().setLayout(dlgFindLayout);
        dlgFindLayout.setHorizontalGroup(
            dlgFindLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(dlgFindLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(dlgFindLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(dlgFindLayout.createSequentialGroup()
                        .addComponent(jLabel4)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(dlgFind_tfFindWhat, javax.swing.GroupLayout.DEFAULT_SIZE, 278, Short.MAX_VALUE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, dlgFindLayout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(dlgFind_btnFind)))
                .addContainerGap())
        );
        dlgFindLayout.setVerticalGroup(
            dlgFindLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(dlgFindLayout.createSequentialGroup()
                .addGap(38, 38, 38)
                .addGroup(dlgFindLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel4)
                    .addComponent(dlgFind_tfFindWhat, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(29, 29, 29)
                .addComponent(dlgFind_btnFind)
                .addContainerGap(31, Short.MAX_VALUE))
        );

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Notepad");
        setResizable(false);

        lblShowPath.setText("File'path");

        btnSearch.setText("Search");
        btnSearch.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSearchActionPerformed(evt);
            }
        });

        taTextArea.setColumns(20);
        taTextArea.setRows(5);
        taTextArea.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                taTextAreaKeyPressed(evt);
            }
        });
        jScrollPane1.setViewportView(taTextArea);

        mFile.setText("File");

        miNew.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_N, java.awt.event.InputEvent.CTRL_MASK));
        miNew.setText("New");
        miNew.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                miNewActionPerformed(evt);
            }
        });
        mFile.add(miNew);

        miOpen.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_O, java.awt.event.InputEvent.CTRL_MASK));
        miOpen.setText("Open");
        miOpen.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                miOpenActionPerformed(evt);
            }
        });
        mFile.add(miOpen);

        miSave.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_S, java.awt.event.InputEvent.CTRL_MASK));
        miSave.setText("Save");
        miSave.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                miSaveActionPerformed(evt);
            }
        });
        mFile.add(miSave);

        miSaveAs.setText("Save As");
        miSaveAs.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                miSaveAsActionPerformed(evt);
            }
        });
        mFile.add(miSaveAs);

        miExit.setText("Exit");
        miExit.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                miExitActionPerformed(evt);
            }
        });
        mFile.add(miExit);

        jmbMenu.add(mFile);

        mEdit.setText("Edit");

        miCut.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_X, java.awt.event.InputEvent.CTRL_MASK));
        miCut.setText("Cut");
        miCut.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                miCutActionPerformed(evt);
            }
        });
        mEdit.add(miCut);

        miCopy.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_C, java.awt.event.InputEvent.CTRL_MASK));
        miCopy.setText("Copy");
        miCopy.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                miCopyActionPerformed(evt);
            }
        });
        mEdit.add(miCopy);

        miPaste.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_V, java.awt.event.InputEvent.CTRL_MASK));
        miPaste.setText("Paste");
        miPaste.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                miPasteActionPerformed(evt);
            }
        });
        mEdit.add(miPaste);

        miFind.setText("Find");
        miFind.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                miFindActionPerformed(evt);
            }
        });
        mEdit.add(miFind);

        jmbMenu.add(mEdit);

        mFormat.setText("Format");

        miColor.setText("Color");
        miColor.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                miColorActionPerformed(evt);
            }
        });
        mFormat.add(miColor);

        miFont.setText("Font...");
        miFont.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                miFontActionPerformed(evt);
            }
        });
        mFormat.add(miFont);

        jmbMenu.add(mFormat);

        setJMenuBar(jmbMenu);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(lblShowPath, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(tfSearch)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnSearch)
                .addContainerGap())
            .addComponent(jScrollPane1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 709, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(btnSearch)
                    .addComponent(tfSearch, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 645, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(lblShowPath, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void miSaveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_miSaveActionPerformed
        if (currentFile == null) {
            return;
        }
        try (PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter(currentFile, false)))) {
            out.print(taTextArea.getText());
            lblShowPath.setText(currentFile.getAbsolutePath());
            // file not modified
            isModified = false;
            // updateStatus();
        } catch (IOException ex) {
            ex.printStackTrace();
            JOptionPane.showMessageDialog(this,
                    ex.getMessage(),
                    "File writing error",
                    JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_miSaveActionPerformed

    private void miNewActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_miNewActionPerformed
//        if (currentFile == null) {
//            taTextArea.setText("");
//            setTitle(defaultTitle);
//            lblShowPath.setText("No file");
//        }
        if (isModified == true) {
            int result = JOptionPane.showConfirmDialog(
                    null,
                    "Do you want to save changes?",
                    "Notepad",
                    JOptionPane.YES_NO_OPTION);
            if (result == JOptionPane.YES_OPTION) {
                if (currentFile != null) {
                    try (PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter(currentFile, true)))) {
                        out.print(taTextArea.getText());
                        taTextArea.setText("");
                        setTitle(defaultTitle);
                        lblShowPath.setText("No file");
                        isModified = false;
                        
                    } catch (IOException e) {
                        JOptionPane.showMessageDialog(this, e.getMessage(),
                                "File not found",
                                JOptionPane.ERROR_MESSAGE);
                    }
                    
                } else {
                    if (fileChooser.showSaveDialog(this) == JFileChooser.APPROVE_OPTION) {
                        File file = fileChooser.getSelectedFile();
                        if (!file.getName().matches(".*\\.[a-zA-Z0-9]{1,}$")) {
                            file = new File(file.getAbsolutePath() + ".txt");
                        }
                        try (PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter(file, false)))) {
                            out.print(taTextArea.getText());
                            // current file changed to the file user selected / created
                            currentFile = file;
                            // file not modified
                            isModified = false;
                            //updateStatus();
                        } catch (IOException ex) {
                            JOptionPane.showMessageDialog(this,
                                    ex.getMessage(),
                                    "File writing error",
                                    JOptionPane.ERROR_MESSAGE);
                        }
                    }
                    
                }
                
            } else {
                taTextArea.setText("");
                setTitle(defaultTitle);
                lblShowPath.setText("No file");
            }
            
        } else {
            taTextArea.setText("");
            setTitle(defaultTitle);
            lblShowPath.setText("No file");
        }

//        miNew.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_N, ActionEvent.CTRL_MASK));//CTRL+N for new JFrame
//        if (isModified == true) {
//            int result = JOptionPane.showConfirmDialog(
//                    null,
//                    "Do you want to save changes?",
//                    "Notepad",
//                    JOptionPane.YES_NO_OPTION);
//            if (result==JOptionPane.YES_OPTION) {
//                
//            }
//            switch (result) {
//                case JOptionPane.YES_OPTION:
////                    if (taTextArea.equals("")) {
////                       saveFile();
////                    } else {
////                        //  saveAs();
////                    }
//                    save(this.taTextArea.getText(), save.getSelectedFile().getPath()));
//                    break;
//                case JOptionPane.NO_OPTION:
//                    newFile();
//                    break;
//                default:
//                    newFile();
//                    break;
//            }
        //       }

    }//GEN-LAST:event_miNewActionPerformed
    

    private void miOpenActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_miOpenActionPerformed
        if (fileChooser.showOpenDialog(this) == JFileChooser.APPROVE_OPTION) {
            
            File file = fileChooser.getSelectedFile();
            
            try {
                Scanner fileInput = new Scanner(file);
                while (fileInput.hasNext()) {
                    fileInput.useDelimiter("\\Z");
                    String line = fileInput.next();
                    taTextArea.setText(line);
                    lblShowPath.setText(file.getAbsolutePath());
                    setTitle(file.getName());
                    currentFile = file;
                    isModified = false;
                }
                
            } catch (IOException e) {
                JOptionPane.showMessageDialog(this, e.getMessage(),
                        "File not found",
                        JOptionPane.ERROR_MESSAGE);
            }
        }
        
        miOpen.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_O, ActionEvent.CTRL_MASK));
    }//GEN-LAST:event_miOpenActionPerformed

    private void taTextAreaKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_taTextAreaKeyPressed
        isModified = true;
        if (currentFile == null) {
            lblShowPath.setText("no file");
            setTitle(defaultTitle);
        } else {
            lblShowPath.setText(currentFile.getAbsolutePath());
            setTitle(currentFile.getName());
        }

    }//GEN-LAST:event_taTextAreaKeyPressed

    private void miExitActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_miExitActionPerformed
        
        setVisible(false);
        dispose();
    }//GEN-LAST:event_miExitActionPerformed

    private void miCutActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_miCutActionPerformed
        String cutSt = taTextArea.getSelectedText();
        StringSelection ss = new StringSelection(cutSt);
        clipboard.setContents(ss, ss);
        taTextArea.replaceRange("", taTextArea.getSelectionStart(), taTextArea.getSelectionEnd());
        

    }//GEN-LAST:event_miCutActionPerformed

    private void miPasteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_miPasteActionPerformed
        try {
            Transferable pastText = clipboard.getContents(TextEditor.this);
            String transfer = (String) pastText.getTransferData(DataFlavor.stringFlavor);
            taTextArea.replaceRange(transfer, taTextArea.getSelectionStart(), taTextArea.getSelectionEnd());
        } catch (UnsupportedFlavorException | IOException e) {
            JOptionPane.showMessageDialog(this, e.getMessage(),
                    "Error in paste file",
                    JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_miPasteActionPerformed

    private void miCopyActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_miCopyActionPerformed
        String coptText = taTextArea.getSelectedText();
        StringSelection copySelection = new StringSelection(coptText);
        clipboard.setContents(copySelection, copySelection);
    }//GEN-LAST:event_miCopyActionPerformed

    private void btnSearchActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSearchActionPerformed
        searchTextArea(taTextArea, tfSearch.getText());
    }//GEN-LAST:event_btnSearchActionPerformed

    private void miSaveAsActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_miSaveAsActionPerformed
        if (fileChooser.showSaveDialog(this) == JFileChooser.APPROVE_OPTION) {
            File file = fileChooser.getSelectedFile();
            if (!file.getName().matches(".*\\.[a-zA-Z0-9]{1,}$")) {
                file = new File(file.getAbsolutePath() + ".txt");
            }
            try (PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter(file, false)))) {
                out.print(taTextArea.getText());
                // current file changed to the file user selected / created
                currentFile = file;
                // file not modified
                isModified = false;
                //updateStatus();
            } catch (IOException ex) {
                ex.printStackTrace();
                JOptionPane.showMessageDialog(this,
                        ex.getMessage(),
                        "File writing error",
                        JOptionPane.ERROR_MESSAGE);
            }
        }
    }//GEN-LAST:event_miSaveAsActionPerformed

    private void miFontActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_miFontActionPerformed
        dlgFont.pack();
        dlgFont.setVisible(true);

    }//GEN-LAST:event_miFontActionPerformed

    private void dlgFontWindowActivated(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_dlgFontWindowActivated
        for (String s : fonts) {
            fontListModel.addElement(s);
        }
        for (String size : sizes) {
            sizeListModel.addElement(size);
        }
        for (String style : styles) {
            styleListModel.addElement(style);
        }
        dlgFont_lstFontSize.setSelectedIndex(0);
        dlgFont_lstFontStyle.setSelectedIndex(0);
    }//GEN-LAST:event_dlgFontWindowActivated

    private void dlgFont_btnCancelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_dlgFont_btnCancelActionPerformed
        dlgFont.setVisible(false);
    }//GEN-LAST:event_dlgFont_btnCancelActionPerformed

    private void dlgFont_txtPreviewActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_dlgFont_txtPreviewActionPerformed

    }//GEN-LAST:event_dlgFont_txtPreviewActionPerformed

    private void dlgFont_lstFontValueChanged(javax.swing.event.ListSelectionEvent evt) {//GEN-FIRST:event_dlgFont_lstFontValueChanged
        try {
            //int currentIndex=dlgFont_lstFont.getSelectedIndex();
            Font f1 = new Font((dlgFont_lstFont.getSelectedValue()),
                    dlgFont_lstFontStyle.getSelectedIndex(), Integer.parseInt((dlgFont_lstFontSize.getSelectedValue())));
            dlgFont_txtPreview.setFont(f1);
        } catch (NumberFormatException e) {
            e.printStackTrace();
            JOptionPane.showMessageDialog(this,
                    e.getMessage(),
                    "error",
                    JOptionPane.ERROR_MESSAGE);
            
        }
    }//GEN-LAST:event_dlgFont_lstFontValueChanged

    private void dlgFont_lstFontMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_dlgFont_lstFontMouseClicked
//        try {
//            //int currentIndex=dlgFont_lstFont.getSelectedIndex();
//            Font f1 = new Font((dlgFont_lstFont.getSelectedValue()),
//                    dlgFont_lstFontStyle.getSelectedIndex(), Integer.parseInt((dlgFont_lstFontSize.getSelectedValue())));
//            dlgFont_txtPreview.setFont(f1);
//        } catch (NumberFormatException e) {
//            e.printStackTrace();
//            JOptionPane.showMessageDialog(this,
//                    e.getMessage(),
//                    "error",
//                    JOptionPane.ERROR_MESSAGE);
//
//        }
    }//GEN-LAST:event_dlgFont_lstFontMouseClicked

    private void dlgFont_lstFontStyleValueChanged(javax.swing.event.ListSelectionEvent evt) {//GEN-FIRST:event_dlgFont_lstFontStyleValueChanged
        try {
            //int currentIndex=dlgFont_lstFont.getSelectedIndex();
            Font f2 = new Font((dlgFont_lstFont.getSelectedValue()),
                    dlgFont_lstFontStyle.getSelectedIndex(), Integer.parseInt((dlgFont_lstFontSize.getSelectedValue())));
            dlgFont_txtPreview.setFont(f2);
        } catch (NumberFormatException e) {
            e.printStackTrace();
            JOptionPane.showMessageDialog(this,
                    e.getMessage(),
                    "error",
                    JOptionPane.ERROR_MESSAGE);
            
        }
    }//GEN-LAST:event_dlgFont_lstFontStyleValueChanged

    private void dlgFont_lstFontSizeValueChanged(javax.swing.event.ListSelectionEvent evt) {//GEN-FIRST:event_dlgFont_lstFontSizeValueChanged
        try {
            //int currentIndex=dlgFont_lstFont.getSelectedIndex();
            Font f3 = new Font((dlgFont_lstFont.getSelectedValue()),
                    dlgFont_lstFontStyle.getSelectedIndex(), Integer.parseInt((dlgFont_lstFontSize.getSelectedValue())));
            dlgFont_txtPreview.setFont(f3);
        } catch (NumberFormatException e) {
            e.printStackTrace();
            JOptionPane.showMessageDialog(this,
                    e.getMessage(),
                    "error",
                    JOptionPane.ERROR_MESSAGE);
            
        }
    }//GEN-LAST:event_dlgFont_lstFontSizeValueChanged

    private void dlgFont_btnOkActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_dlgFont_btnOkActionPerformed
        Font f2 = new Font((dlgFont_lstFont.getSelectedValue()),
                dlgFont_lstFontStyle.getSelectedIndex(), Integer.parseInt((dlgFont_lstFontSize.getSelectedValue())));
        taTextArea.setFont((f2));
        dlgFont.setVisible(false);
    }//GEN-LAST:event_dlgFont_btnOkActionPerformed
    Color currentColor = Color.BLACK;
    private void miColorActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_miColorActionPerformed
        Color newColor = JColorChooser.showDialog(this, "Choose font color", currentColor);
        if (newColor != null) {
            currentColor = newColor;
            taTextArea.setForeground(currentColor);
        }
    }//GEN-LAST:event_miColorActionPerformed

    private void miFindActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_miFindActionPerformed
        dlgFind.setVisible(true);
        dlgFind.pack();
    }//GEN-LAST:event_miFindActionPerformed

    private void dlgFind_btnFindActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_dlgFind_btnFindActionPerformed
       searchTextArea(taTextArea, dlgFind_tfFindWhat.getText());
    }//GEN-LAST:event_dlgFind_btnFindActionPerformed
    
    class Highlight extends DefaultHighlighter.DefaultHighlightPainter {
        
        public Highlight(Color c) {
            super(c);
        }
        
    }
    DefaultHighlighter.HighlightPainter highlighter = new Highlight(Color.blue);
    
    public void removeHighLight(JTextComponent textComp) {
        Highlighter remoHighlighter = textComp.getHighlighter();
        Highlighter.Highlight[] remove = remoHighlighter.getHighlights();
        for (int i = 0; i < remove.length; i++) {
            if (remove[i].getPainter() instanceof Highlighter) {
                remoHighlighter.removeHighlight(remove[i]);
            }
        }
    }
    
    public void searchTextArea(JTextComponent texCom, String textStr) {
        removeHighLight(texCom);
        try {
            Highlighter hilight = texCom.getHighlighter();
            Document doc = texCom.getDocument();
            String text = doc.getText(0, doc.getLength());
            int pos = 0;
            while ((pos = text.toUpperCase().indexOf(textStr.toUpperCase(), pos)) >= 0) {
                hilight.addHighlight(pos, pos + textStr.length(), highlighter);
                pos += textStr.length();
            }
        } catch (BadLocationException e) {
        }
    }
    
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(TextEditor.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(TextEditor.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(TextEditor.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(TextEditor.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new TextEditor().setVisible(true);
            }
        });
    }

    //set icon for JFrame
    private void initIcon() {
        setIconImage(Toolkit.getDefaultToolkit().getImage(getClass().getResource("icon.png")));
    }
    
    public void newFile() {
        taTextArea.setText("");
        this.setTitle(defaultTitle);
        lblShowPath.setText("No file");
        isModified = false;
    }
    
    public void saveFile() {
        if (fileChooser.showSaveDialog(this) == JFileChooser.APPROVE_OPTION) {
            File file = fileChooser.getSelectedFile();
            if (file.getAbsoluteFile() != null) {
                file = new File(file.getAbsolutePath() + ".txt");
                setTitle(file + "");
            }
            try (PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter(file, false)))) {
                out.print(taTextArea.getText());
                currentFile = file;
                isModified = false;
                lblShowPath.setText(currentFile.getAbsolutePath());
                setTitle(currentFile.getName());
            } catch (IOException e) {
                JOptionPane.showMessageDialog(this, e.getMessage(),
                        "File not found",
                        JOptionPane.ERROR_MESSAGE);
            }
        }
    }
    
    public Font font() {
        Font font = new Font((dlgFont_lstFont.getSelectedValue()),
                dlgFont_lstFontStyle.getSelectedIndex(), Integer.parseInt((dlgFont_lstFontSize.getSelectedValue())));
        return font;
    }


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnSearch;
    private javax.swing.JColorChooser colorChooser;
    private javax.swing.JDialog dlgFind;
    private javax.swing.JButton dlgFind_btnFind;
    private javax.swing.JTextField dlgFind_tfFindWhat;
    private javax.swing.JDialog dlgFont;
    private javax.swing.JButton dlgFont_btnCancel;
    private javax.swing.JButton dlgFont_btnOk;
    private javax.swing.JList<String> dlgFont_lstFont;
    private javax.swing.JList<String> dlgFont_lstFontSize;
    private javax.swing.JList<String> dlgFont_lstFontStyle;
    private javax.swing.JTextField dlgFont_txtPreview;
    private javax.swing.JFileChooser fileChooser;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JScrollPane jScrollPane4;
    private javax.swing.JMenuBar jmbMenu;
    private javax.swing.JLabel lblShowPath;
    private javax.swing.JMenu mEdit;
    private javax.swing.JMenu mFile;
    private javax.swing.JMenu mFormat;
    private javax.swing.JMenuItem miColor;
    private javax.swing.JMenuItem miCopy;
    private javax.swing.JMenuItem miCut;
    private javax.swing.JMenuItem miExit;
    private javax.swing.JMenuItem miFind;
    private javax.swing.JMenuItem miFont;
    private javax.swing.JMenuItem miNew;
    private javax.swing.JMenuItem miOpen;
    private javax.swing.JMenuItem miPaste;
    private javax.swing.JMenuItem miSave;
    private javax.swing.JMenuItem miSaveAs;
    private javax.swing.JTextArea taTextArea;
    private javax.swing.JTextField tfSearch;
    // End of variables declaration//GEN-END:variables
}
